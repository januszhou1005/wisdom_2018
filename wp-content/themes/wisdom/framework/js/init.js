// JavaScript Document
var count_editors = 0;

jQuery(document).ready(function(e) {
	jQuery(".list-elements").sortable({ 
		placeholder: "sort-articles",
		revert: false,
		tolerance: 'pointer',
		start: function(event, ui) {
			ui.placeholder.height(ui.item.height());
		},
		stop: function(e, ui) {
			remove_fix_specs();
		}
	});
	
	jQuery(".list-sub-elements").sortable({ 
		placeholder: "sort-articles",
		revert: false,
		tolerance: 'pointer',
		start: function(event, ui) {
			ui.placeholder.height(ui.item.height());
		},
		stop: function(e, ui) {
			remove_fix_specs();
		}
	});
	
	jQuery(".list-sub-sub-elements").sortable({ 
		placeholder: "sort-products",
		revert: false,
		tolerance: 'pointer',
		start: function(event, ui) {
			ui.placeholder.height(ui.item.height());
		},
		stop: function(e, ui) {
			remove_fix_specs();
		}
	});
		
	jQuery('.remove-section').live('click', function(e) {
		e.preventDefault();
		
		jQuery(this).parents('.rounded-div').remove();
	});
		
	jQuery('.remove-section-var').live('click', function(e) {
		e.preventDefault();
		
		jQuery(this).parents('.root-variations').remove();
		
		remove_fix_specs();
	});
		
	jQuery('.remove-section-color').live('click', function(e) {
		e.preventDefault();
		
		jQuery(this).parents('.root-colors').remove();
		
		remove_fix_specs();
	});
		
	jQuery('.remove-section-image').live('click', function(e) {
		e.preventDefault();
		
		jQuery(this).parents('.section-image').remove();
		
		remove_fix_specs();
	});

	jQuery('.upload-image').live('click', function(e) {
		e.preventDefault();
		
		obj_image = jQuery(this);
			 
		frame = wp.media({
			title : 'Select an image',
			multiple : false,
			library : { type : 'image'},
			button : { text : 'Insert image' },
		});
		  	
		frame.on('close',function() {
			var selection = frame.state().get('selection');
			
			selection.each(function(attachment) {
				var image_url = attachment["attributes"].url;
				var image_id  = attachment["attributes"].id;
				
				obj_image.prev().val(image_url);
				obj_image.next().next().val(image_id);
			});
		});
			
		frame.open();
  	});

	jQuery('.add-variation-color-image').live('click', function(e) {
		e.preventDefault();
		
		obj_image = jQuery(this);
			 
		frame = wp.media({
			title : 'Select images',
			multiple : true,
			library : { type : 'image'},
			button : { text : 'Insert images' },
		});
		  	
		frame.on('close',function() {
			var selection = frame.state().get('selection');
			
			this_length 	   = obj_image.parents('.root-variations').attr('data-num');
			this_color_length = obj_image.parents('.root-colors').attr('data-num');
			console.log(this_color_length);
			if (this_color_length == undefined) {
				this_color_length = 0;	
			}
			
			selection.each(function(attachment) {
				var image_url = attachment["attributes"].url;
				var image_id  = attachment["attributes"].id;
				
				html = '<div class="section-image" style="width: 100px; margin-right: 10px; margin-bottom: 10px; float: left; text-align: center; background-color: #fff;"> \
							<img src="' + image_url + '" width="100px"> \
							<input class="color_image" name="variation[name][' + this_length + '][colors][' + this_color_length + '][images][]" type="hidden" value="' + image_id + '"> \
							<a href="#" class="remove-section-image">Delete</a> \
						</div>'
				obj_image.parents('.root-colors').find('.list-sub-sub-elements').append(html);
			});
			
		});
			
		frame.open();
  	});

	jQuery('.add-slide').click(function(e) {
		e.preventDefault();
		
		this_length 	   = jQuery(this).parents('.root-variations').attr('data-num');
		this_color_length = jQuery(this).parents('.root-variations').find('.root-colors').length;
		
		jQuery(this).parent().next().append('<div class="rounded-div" style="padding-bottom: 10px;"> \
												<div> \
													<input name="slider_slide[]" type="text" style="width: 450px"> \
													<input type="button" class="button button-primary upload-image" value="Upload image"> \
													<input type="button" class="button remove-section" value="Remove"> \
													<input name="slide[image][]" type="hidden"> \
												</div> \
											</div>');

	});

	jQuery('.add-variation').click(function(e) {
		e.preventDefault();
		
		var_length  = jQuery('.product-variations .root-variations').length;
		this_length = var_length;
		
		jQuery(this).parent().next().append('<div class="rounded-div root-variations" style="padding-bottom: 10px;" data-num="' + this_length + '"> \
												<div> \
													<input class="var-name" name="variation[name][' + this_length + '][title]" type="text" style="width: 400px"> \
													<input type="button" class="button button-primary add-variation-color" value="Add Color"> \
													<input type="button" class="button remove-section-var" value="Remove Variation"> \
													<p><strong>Available Colors</strong></p> \
													<div class="list-sub-elements" style="margin-top: 10px"></div> \
												</div> \
											</div>');

		jQuery(".list-sub-elements").sortable({ 
			placeholder: "sort-articles",
			revert: false,
			tolerance: 'pointer',
			start: function(event, ui) {
				ui.placeholder.height(ui.item.height());
			},
			stop: function(e, ui) {
				remove_fix_specs();
			}
		});
	});

	jQuery('.add-variation-color').live('click', function(e) {
		e.preventDefault();
		
		this_length 	   = jQuery(this).parents('.root-variations').attr('data-num');
		this_color_length = jQuery(this).parents('.root-variations').find('.root-colors').length - 1;
		this_color_length = this_color_length + 1;
		
		jQuery(this).parents('.root-variations').find('.list-sub-elements').append(' \
									<div class="rounded-div root-colors" style="padding-bottom: 10px;background-color: #fff;background-image: none;" data-num="' + this_color_length + '"> \
										<div> \
											<input class="var-color" name="variation[name][' + this_length + '][colors][' + this_color_length + '][title]" type="text" style="width: 400px"> \
											<input type="button" class="button button-primary add-variation-color-image" value="Add Image"> \
											<input type="button" class="button remove-section-color" value="Remove Color"> \
											<p><strong>Images</strong></p> \
											<div class="list-sub-sub-elements"></div> \
											<div style="clear: both"></div> \
										</div> \
									</div>');


		jQuery(".list-sub-sub-elements").sortable({ 
			placeholder: "sort-products",
			revert: false,
			tolerance: 'pointer',
			start: function(event, ui) {
				ui.placeholder.height(ui.item.height());
			},
			stop: function(e, ui) {
				remove_fix_specs();
			}
		});
	});
});

function remove_fix_specs() {
	count_vars = -1;
	
	jQuery('.root-variations').each(function(index, element) {
		count_vars++;
		
		obj_var = jQuery(this);
		
		obj_var.attr('data-num', count_vars);
		obj_var.find('.var-name').attr('name', 'variation[name][' + count_vars + '][title]'); 
		
		count_colors = -1;
		
		obj_var.find('.root-colors').each(function(index, element) {
			count_colors++;
			
			obj_color = jQuery(this);
			obj_var.attr('data-num', count_colors);
			
			obj_color.find('.var-color').attr('name', 'variation[name][' + count_vars + '][colors][' + count_colors + '][title]');
			
			obj_color.find('.color_image').each(function(index, element) {
				obj_image = jQuery(this);
				
				obj_image.attr('name', 'variation[name][' + count_vars + '][colors][' + count_colors + '][images][]');
			}); 
		});	
	});	
}