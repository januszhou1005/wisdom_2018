<?php

	get_header();

?>
<div class="homeHero" style="background-image:url('<?php the_field('top_banner'); ?>');">

	<div class="homeHeroContent">
		<h1><?php the_field('top_banner_title'); ?></h1>
		<h4><?php the_field('top_banner_sub-title'); ?></h4>
		<a href="/shop">
			<button>Browse All</button>
		</a>
	</div>
</div>

<div class="homeSectionOne" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/gold-rings.png');">
	<div class="homeSectionOneLeft">

	</div>
	<div class="homeSectionOneRight">
		<div class="homeSectionOneRightText">
			<img class="dash" src="<?php echo get_bloginfo('template_url'); ?>/pics/gold-dash.png">

			<h4>One of a kind variety,<br> for every type of cabinet</h4>
			<p>Wisdom Stone is a unique new brand of decorative cabinet hardware with over 300 different styles in a variety of finishes and colored crystals. You won’t find hardware quite like it.</p>
			<a href="/about-us">Learn More <img class="rightArrow" src="<?php echo get_bloginfo('template_url'); ?>/pics/right-arrow-purp.png"></a>
		</div>
	</div>
</div>

<div class="homeSectionTwo" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/purple-rings.png');">
	<div class="homeSectionTwoLeft" style="background-image:url(<?php the_field('lasting_quality_banner'); ?>);">
		<div class="homeSectionTwoLeftText">
			<h3>Lasting Quality</h3>
			<p>Wisdom Stone is a unique new brand of decorative cabinet hardware with over 300 different styles in a variety of finishes and colored crystals. You won’t find hardware quite like it.</p>
			<a href="/shop">View Products</a>
		</div>
	</div>
	<div class="homeSectionTwoRight">

	</div>
</div>

<div class="newsletterSection">
	<div class="newsletterSectionWrapper">
		<h5>Get our latest products and expert design tips right in your inbox</h5>
		<form class="newsletterForm">
            <?php echo do_shortcode('[wysija_form id="1"]'); ?>
		</form>
	</div>
</div>

<div class="midHero" style="background-image:url('<?php the_field('bottom_banner'); ?>');">
	<div class="midHeroBorder">
		<div class="midHeroContent">
			<h1><?php the_field('bottom_banner_title'); ?></h1>
			<h4><?php the_field('bottom_banner_sub-title'); ?></h4>
			<a href="/about-us">
				<button>Learn More</button>
			</a>
		</div>
	</div>
</div>

<div class="sectionShowcase">
	<div class="sectionShowcaseContainer">
		<div class="sectionShowcaseHeader">
			<h2>Cabinetry adornments for your every need</h2>
			<p>We offer a wide variety of decorative hardware for many home styles. Browse our extensive options of beautiful cabinet hardware to find the design that works for you and your home. </p>
		</div>
		<div class="sectionShowcaseContent">
			<img class="knob" src="<?php echo get_bloginfo('template_url'); ?>/pics/knob-stick.png"> 
			<div class="topSection" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/knob-showcase.jpg');">
				<div class="topSectionLeft">
					<h4>Knobs</h4>
					<h6><?php the_field('knobs_subtitle'); ?></h6>
					<p><?php the_field('knobs_text'); ?></p>
					<a href="/product_category/drawer-knobs/">Learn More</a>
				</div>
				<div class="topSectionRight">
					<div class="topSectionRightBorder">

					</div>
				</div>
			</div>
			<div class="bottomSection" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/pull-showcase.jpg');">
				<div class="bottomSectionLeft">
				</div>
				<div class="bottomSectionRight">
					<h4>Pulls</h4>
					<h6><?php the_field('pulls_subtitle'); ?></h6>
					<p><?php the_field('pulls_text'); ?></p>
					<a href="/product_category/drawer-pulls/">Learn More</a>
				</div>
			</div>
			<img class="pull" src="<?php echo get_bloginfo('template_url'); ?>/pics/pull-stick.png"> 
		</div>
		<div class="showcaseRetailers">
			<img class="dash" src="<?php echo get_bloginfo('template_url'); ?>/pics/gold-dash.png">
			<h4>Available at these Retailers</h4>
			<div class="retailerIcons">
				<div>
					<a href="<?php the_field('retailer_link_1'); ?>" target="_blank">
						<img src="<?php the_field('logo_1'); ?>">
					</a>
				</div>
				<div>
					<a href="<?php the_field('retailer_link_2'); ?>" target="_blank">
						<img src="<?php the_field('logo_2'); ?>">
					</a>
				</div>
				<div>
					<a href="<?php the_field('retailer_link_3'); ?>" target="_blank">
						<img src="<?php the_field('logo_3'); ?>">
					</a>
				</div>
				<div>
					<a href="<?php the_field('retailer_link_4'); ?>" target="_blank">
						<img src="<?php the_field('logo_4'); ?>">
					</a>
				</div>
				<div>
					<a href="<?php the_field('retailer_link_5'); ?>" target="_blank">
						<img src="<?php the_field('logo_5'); ?>">
					</a>
				</div>
				<div>
					<a href="<?php the_field('retailer_link_6'); ?>" target="_blank">
						<img src="<?php the_field('logo_6'); ?>">
					</a>
				</div>
			</div>
			<div class="retailerIconsMobile" style="display:none;">
				<div>
					<a href="<?php the_field('retailer_link_1'); ?>" target="_blank">
						<img src="<?php the_field('logo_1'); ?>">
					</a>
				</div>
				<div>
					<a href="<?php the_field('retailer_link_2'); ?>" target="_blank">
						<img src="<?php the_field('logo_2'); ?>">
					</a>
				</div>
				<div>
					<a href="<?php the_field('retailer_link_3'); ?>" target="_blank">
						<img src="<?php the_field('logo_3'); ?>">
					</a>
				</div>
				<div>
					<a href="<?php the_field('retailer_link_4'); ?>" target="_blank">
						<img src="<?php the_field('logo_4'); ?>">
					</a>
				</div>
				<div>
					<a href="<?php the_field('retailer_link_5'); ?>" target="_blank">
						<img src="<?php the_field('logo_5'); ?>">
					</a>
				</div>
				<div>
					<a href="<?php the_field('retailer_link_6'); ?>" target="_blank">
						<img src="<?php the_field('logo_6'); ?>">
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $('.retailerIconsMobile').slick({

  });
});
		
</script>

<?php
	
	get_footer();
?>