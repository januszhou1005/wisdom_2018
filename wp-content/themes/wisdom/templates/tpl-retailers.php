<?php
	/* Template name: Retailers */
	get_header('shop');
	
	the_post();
?>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<main role="main"> 
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
		<div class="faqSection">
			<div class="faqGrid">
				
					<div class="faqGridHeader">
						<h5>Browse</h5>
						<h2>Online Retailers</h2>
						<img src="<?php echo get_bloginfo('template_url'); ?>/pics/2-pulls.png">
					</div>

					<div class="retailerGrid">
						<div class="retailerGridWrapper">
							<div class="retailerGridLeft">
								<p>Would you like to become a <span>Wisdom Stone</span> online retailer?</p>
								<h6><a href="/contact">Contact Us</a></h6>
							</div>
<!-- 							<div class="retailerGridRight">
								<p><strong>49</strong> Stores Found</p>
							</div> -->
						<ul>
						<?php
                                $args = array(
                          'post_type' => 'online_retailers',
                          'posts_per_page' => '20'
                          );
                                $products = new WP_Query( $args );
                                      if( $products->have_posts() ) {
                                while( $products->have_posts() ) {
                                $products->the_post();
                          ?>
							<li>
								<h3><?php the_field('retailer'); ?></h3>
								<p><?php the_field('web_address'); ?></p>
								<h4><a href="http://<?php the_field('web_address'); ?>" target="_blank">Visit The Website</a></h4>
							</li>
						<?php
                                }
                                      }
                                else {
                                echo '';
                                }
                          ?>
<!-- 							<li>
								<h3>125west.com</h3>
								<p>www.125west.com</p>
								<h4><a href="http://www.125west.com" target="_blank">Visit The Website</a></h4>
							</li>
							<li>
								<h3>Bellacor</h3>
								<p>www.bellacor.com</p>
								<h4><a href="http://www.bellacor.com" target="_blank">Visit The Website</a></h4>
							</li>
							<li>
								<h3>Comfort House</h3>
								<p>www.comforthouse.com</p>
								<h4><a href="http://www.comforthouse.com" target="_blank">Visit The Website</a></h4>
							</li>
							<li>
								<h3>Daily Grommet</h3>
								<p>www.thegrommet.com</p>
								<h4><a href="http://www.thegrommet.com" target="_blank">Visit The Website</a></h4>
							</li>
							<li>
								<h3>Design Hardware</h3>
								<p>www.designhardware.com</p>
								<h4><a href="http://www.designhardware.com" target="_blank">Visit The Website</a></h4>
							</li>
							<li>
								<h3>CabinetsRWe</h3>
								<p>www.cabinetsrwe.com</p>
								<h4><a href="http://www.cabinetsrwe.com" target="_blank">Visit The Website</a></h4>
							</li>
							<li>
								<h3>Roch Enterprises</h3>
								<p>www.rochenterprises.com</p>
								<h4><a href="http://www.rochenterprises.com" target="_blank">Visit The Website</a></h4>
							</li>
							<li>
								<h3>Open Cabinet</h3>
								<p>www.opencabinet.com</p>
								<h4><a href="http://www.opencabinet.com" target="_blank">Visit The Website</a></h4>
							</li> -->
						</ul>
					</div>
				</div>
			</div>
		</div>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ -->
</main>
<?php
	
	get_footer();
?>