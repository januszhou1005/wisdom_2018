<?php
  /* Template name: Gallery */
  get_header('shop');
  
  the_post();
?>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *MAIN CONTENT
   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->

<style>
.row > .column {
  padding: 0 8px;
}
footer {
  display: none !important;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

.column {
  float: left;
  width: 25%;
}



/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

.cursor {
  cursor: pointer
}



/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}



/* Number text (1/3 etc) */


img {
  margin-bottom: -4px;

}


.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
}
    .galleryHeader .addtoany_shortcode {
        background: #fff;
    }
</style>

    <div class="gallerySection">
      <div class="galleryHeader" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/galleryHeader.png');">
        <h1>Inspiration</h1><?php echo do_shortcode('[addtoany url="wisdom-stone.com" title="Come See Our New Gallery at"]'); ?>

      </div>
<div class="row imageGrid" id="ms-container">
    <?php
            $args = array(
      'post_type' => 'gallery_images',
      'posts_per_page' => '99'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
  <div class="column ms-item ">

    <img src="<?php the_field('image'); ?>" style="width:100%" onclick="openModal();currentSlide(<?php the_field('id'); ?>)" class="hover-shadow cursor">

  </div>
          <?php
            }
                  }
            else {
            echo 'No Team Members Found';
            }
      ?>


<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div id="modal-content">

  

            <?php
            $args = array(
      'post_type' => 'gallery_images',
      'posts_per_page' => '99'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>

    <div class="mySlides">
      <div class="social_share">
        <?php echo do_shortcode('[addtoany url="wisdom-stone.com/inspiration"]' ); ?>
      </div>
      <div class="numbertext"><?php the_field('title'); ?></div>
      <img class="slideImage" src="<?php the_field('image'); ?>" style="width:100%">
    </div>
  <?php
            }
                  }
            else {
            echo 'No Team Members Found';
            }
      ?>  

    
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>

<!--            <//?php
            $args = array(
      'post_type' => 'gallery_images',
      'posts_per_page' => '99'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?> -->
<!--     <div class="column">
      <img class="demo cursor" src="<?php the_field('image'); ?>" style="width:100%" onclick="currentSlide(<?php the_field('id'); ?>)" alt="">
    </div> -->
<!--      <//?php
            }
                  }
            else {
            echo 'No Team Members Found';
            }
      ?>  
 -->
  </div>
</div>
</div>

<script>
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

    <script type="text/javascript">
        
        jQuery(window).load(function() {
      var container = document.querySelector('#ms-container');
      var msnry = new Masonry( container, {
        itemSelector: '.ms-item',
        columnWidth: '.ms-item',                
      });  
      
        });

      
    </script>
   

<?php
  
  get_footer();
?>